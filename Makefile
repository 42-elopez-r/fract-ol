DEF_MAX_ITERATIONS = 250
WIDTH = 400
HEIGHT = 400

CC = gcc
SHELL = /bin/sh
CFLAGS += -I include -Wall -Werror -Wextra -O2 -D PROG_NAME=\"fractol\" \
		  -D DEF_MAX_ITERATIONS=$(DEF_MAX_ITERATIONS) -D WIDTH=$(WIDTH) \
		  -D HEIGHT=$(HEIGHT)

ifeq ($(shell uname), Linux)
LINUX_DEF = -D LINUX
CFLAGS += $(LINUX_DEF)
LDFLAGS += -lXext -lX11
else
LDFLAGS += -framework OpenGL -framework AppKit
endif

NAME = fractol

SRCS:= $(shell find src -name "*.c")
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): libmlx.a $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(NAME) $(OBJS) libmlx.a

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

libmlx.a:
	if [ $(shell uname) == Linux ]; then \
		if [ ! -d minilibx-linux ]; then \
			git clone https://github.com/42Paris/minilibx-linux; \
			cp minilibx-linux/mlx.h include; \
		fi; \
		$(MAKE) -C minilibx-linux; \
		cp minilibx-linux/libmlx.a .; \
	else \
		cp /usr/local/include/mlx.h include; \
		cp /usr/local/lib/libmlx.a .; \
		sed -i '' '/mlx_destroy_display/d' src/minilibx/screen_init_delete.c; \
	fi

clean:
	find . -name "*.o" -delete -print
	rm -rf minilibx-linux
	rm -f include/mlx.h
	rm -f libmlx.a

fclean: clean
	rm -f $(NAME)

re: fclean all

bonus: all

debug: CFLAGS = -I include -Wall -Werror -Wextra -D PROG_NAME=\"fractol\" \
		  -D DEF_MAX_ITERATIONS=$(DEF_MAX_ITERATIONS) -D WIDTH=$(WIDTH) \
		  -D HEIGHT=$(HEIGHT) $(LINUX_DEF) -g
debug: fclean $(NAME)

.PHONY: all clean fclean re debug
