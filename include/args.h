/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   args.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/31 18:17:31 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/03 13:18:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARGS_H
# define ARGS_H

# include <t_complex.h>
# include <fractal_types.h>
# include <stdbool.h>

struct				s_args
{
	enum e_fractal	type;
	t_complex		julia_c;
};

bool				parse_args(int argc, char *argv[], struct s_args *args);

#endif
