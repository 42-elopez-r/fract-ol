/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal_types.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/03 13:17:31 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:31:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTAL_TYPES_H
# define FRACTAL_TYPES_H

enum e_fractal {MANDELBROT, JULIA, BRANDELMOT};

#endif
