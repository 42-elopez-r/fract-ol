/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 12:58:05 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:41:47 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <minilibx.h>
# include <t_complex.h>
# include <fractal_types.h>
# include <args.h>
# include <stdbool.h>

struct	s_fractol
{
	t_screen		*screen;
	enum e_fractal	fractal_type;
	int				h_offset;
	int				v_offset;
	long double		zoom;
	bool			inverted_colors;
	unsigned int	max_iterations;
	t_complex		julia_c;
	long double		lambda_brandel;
	unsigned int	*color_palette;
};

/*
 * Public use
 */
struct s_fractol	*init_fractol(struct s_args *args);
void				update_fractal(struct s_fractol *fractol);
void				delete_fractol(struct s_fractol *fractol);
int					key_event(int keycode, struct s_fractol *fractol);
int					mouse_event(int button, int x, int y,
						struct s_fractol *fractol);

/*
 * Internal use
 */
int					mandelbrot_divergence(unsigned int x, unsigned int y,
						struct s_fractol *fractol);
int					brandelmot_divergence(unsigned int x, unsigned int y,
						struct s_fractol *fractol);
int					julia_divergence(unsigned int x, unsigned int y,
						struct s_fractol *fractol);
void				draw_pixel(int divergence, unsigned int x, unsigned int y,
						struct s_fractol *fractol);
bool				regenerate_color_palette(struct s_fractol *fractol);

#endif
