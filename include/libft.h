/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 20:45:58 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/03 20:33:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

/*
** INCLUDES:
** stddef.h: size_t type
*/

# include <stddef.h>

/*
** Reimplementations from the Libft with the Norm 3
*/

void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(const char *s, int fd);
void			ft_putendl_fd(const char *s, int fd);
size_t			ft_strlen(const char *s);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
int				ft_strcmp(const char *s1, const char *s2);
void			*ft_calloc(size_t nmemb, size_t size);
int				ft_atoi(const char *nptr);
long long		ft_atoll(const char *nptr);
char			*ft_itoa(int n);
int				ft_isdigit(int c);
void			ft_bzero(void *s, size_t n);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
double			ft_atof(const char *str);
char			*ft_strdup(const char *s);
long			tern_long(int condition, long if_true, long if_false);
int				tern_int(int condition, int if_true, int if_false);
unsigned int	tern_uint(int condition, unsigned int if_true,
					unsigned int if_false);
size_t			tern_size_t(int condition, size_t if_true, size_t if_false);
char			*tern_str(int condition, char *if_true, char *if_false);
int				ft_max_int(int a, int b);

#endif
