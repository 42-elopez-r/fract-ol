/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linux_keys.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 17:31:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 16:18:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LINUX_KEYS_H
# define LINUX_KEYS_H

# define LEFT_KEY 0xff51
# define UP_KEY 0xff52
# define RIGHT_KEY 0xff53
# define DOWN_KEY 0xff54
# define NINE_KEY 57
# define ZERO_KEY 48
# define MINUS_KEY 45
# define EQUALS_KEY 61
# define I_KEY 105
# define ESC_KEY 65307
# define K_KEY 107
# define L_KEY 108

#endif
