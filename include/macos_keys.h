/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   macos_keys.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 17:31:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:44:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MACOS_KEYS_H
# define MACOS_KEYS_H

# define LEFT_KEY 123
# define UP_KEY 126
# define RIGHT_KEY 124
# define DOWN_KEY 125
# define NINE_KEY 25
# define ZERO_KEY 29
# define MINUS_KEY 27
# define EQUALS_KEY 24
# define I_KEY 34
# define ESC_KEY 53
# define K_KEY 40
# define L_KEY 37

#endif
