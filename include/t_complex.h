/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_complex.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/31 18:29:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/03 13:14:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_COMPLEX_H
# define T_COMPLEX_H

# include <stdbool.h>

typedef struct s_complex
{
	long double	r;
	long double	i;
}				t_complex;

bool	is_module_smaller_or_equal_than_two(const t_complex *c);

#endif
