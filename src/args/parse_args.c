/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/31 20:28:05 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:33:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <args.h>
#include <libft.h>
#include <stdio.h>

/*
 * This function checks that the passed string starts with a digit (or a -
 * followed by a digit) and it contains one dot.
 */
static bool	is_a_float_more_or_less(const char *str)
{
	if (!ft_isdigit(*str) && *str != '-')
		return (false);
	if (str[0] == '-' && !ft_isdigit(str[1]))
		return (false);
	if (!ft_strchr(str, '.') || ft_strchr(str, '.') != ft_strrchr(str, '.'))
		return (false);
	if (ft_strlen(str) > 10)
		return (false);
	return (true);
}

/*
 * This funcion parses a complex number from its two corresponding CLI
 * arguments, storing it into c and returning true if success.
 */
static bool	parse_complex(t_complex *c, char *c_argv[])
{
	if (!is_a_float_more_or_less(c_argv[0]))
	{
		printf("Invalid number: %s\n", c_argv[0]);
		return (false);
	}
	if (!is_a_float_more_or_less(c_argv[1]))
	{
		printf("Invalid number: %s\n", c_argv[1]);
		return (false);
	}
	c->r = ft_atof(c_argv[0]);
	c->i = ft_atof(c_argv[1]);
	return (true);
}

/*
 * This function parses the CLI arguments and stores them into the struct
 * args. If the arguments were invalid it returns false.
 */
bool	parse_args(int argc, char *argv[], struct s_args *args)
{
	if (argc < 2
		|| ((ft_strcmp(argv[1], "m") != 0 && ft_strcmp(argv[1], "j") != 0
				&& ft_strcmp(argv[1], "b") != 0))
		|| (((argv[1][0] == 'm' || argv[1][0] == 's') && argc != 2)
		|| (argv[1][0] == 'j' && argc != 4)))
	{
		printf("USAGE: %s [ m | j C_REAL C_IMAGINARY ]\n", argv[0]);
		return (false);
	}
	if (argv[1][0] == 'm' || argv[1][0] == 'b')
	{
		args->type = tern_int(argv[1][0] == 'm', MANDELBROT, BRANDELMOT);
		args->julia_c.r = 0;
		args->julia_c.i = 0;
	}
	else
	{
		args->type = JULIA;
		if (!parse_complex(&args->julia_c, argv + 2))
			return (false);
	}
	return (true);
}
