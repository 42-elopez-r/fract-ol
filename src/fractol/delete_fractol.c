/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_fractol.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 16:46:53 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/04 21:57:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <stdlib.h>

/*
 * This function deallocates the memory corresponding to a struct s_fractol.
 */
void	delete_fractol(struct s_fractol *fractol)
{
	if (fractol)
	{
		delete_screen(fractol->screen, 0);
		free(fractol->color_palette);
		free(fractol);
	}
}
