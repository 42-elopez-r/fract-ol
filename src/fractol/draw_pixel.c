/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_pixel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 18:06:07 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/03 13:22:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <libft.h>

/*
 * This function changes the color of the passed coordinates to one
 * matching its divergence.
 */
void	draw_pixel(int divergence, unsigned int x, unsigned int y,
		struct s_fractol *fractol)
{
	if (divergence < 0)
		fractol->screen->buffer[x][y] = 0;
	else
		fractol->screen->buffer[x][y] = fractol->color_palette[divergence];
}
