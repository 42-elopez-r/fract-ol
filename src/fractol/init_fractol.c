/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fractol.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 16:24:43 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:42:15 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <libft.h>
#include <stdlib.h>

/*
 * This function initializes some of the struct s_fractol members.
 */
static void	init_fractol_trivial(struct s_fractol *fractol, struct s_args *args)
{
	fractol->fractal_type = args->type;
	fractol->h_offset = 0;
	fractol->v_offset = 0;
	fractol->zoom = 1.0L;
	fractol->inverted_colors = false;
	fractol->max_iterations = DEF_MAX_ITERATIONS;
	fractol->julia_c = args->julia_c;
	fractol->lambda_brandel = 0.2L;
	fractol->color_palette = NULL;
}

/*
 * This function initializes a struct s_fractol (given the passed CLI
 * arguments). Returns NULL in case of error.
 */
struct s_fractol	*init_fractol(struct s_args *args)
{
	struct s_fractol	*fractol;

	fractol = malloc(sizeof(struct s_fractol));
	if (!fractol)
	{
		ft_putendl_fd("Memory allocation error", 2);
		return (NULL);
	}
	init_fractol_trivial(fractol, args);
	if (!regenerate_color_palette(fractol))
	{
		free(fractol);
		return (NULL);
	}
	fractol->screen = init_screen(WIDTH, HEIGHT);
	if (!fractol->screen)
	{
		free(fractol->color_palette);
		free(fractol);
		return (NULL);
	}
	return (fractol);
}
