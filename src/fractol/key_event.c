/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/03 17:39:47 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 20:36:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <minilibx.h>
#include <libft.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Ya know, the Norm.
 */
static void	key_event_and_the_azkaban_prisioner(int keycode,
		struct s_fractol *fractol)
{
	if (keycode == K_KEY)
	{
		fractol->lambda_brandel -= 0.05L;
		printf("Brandelmot lambda: %.2Lf\n", fractol->lambda_brandel);
	}
	else if (keycode == L_KEY)
	{
		fractol->lambda_brandel += 0.05L;
		printf("Brandelmot lambda: %.2Lf\n", fractol->lambda_brandel);
	}
	else
		printf("%d\n", keycode);
}

/*
 * Ya know, the Norm.
 */
static void	key_event_and_the_secrets_chamber(int keycode,
		struct s_fractol *fractol)
{
	if (keycode == NINE_KEY)
	{
		fractol->max_iterations -= tern_uint(
				(long)fractol->max_iterations - 50 > 0, 50, 0);
		regenerate_color_palette(fractol);
		printf("Max iterations: %u\n", fractol->max_iterations);
	}
	else if (keycode == ZERO_KEY)
	{
		fractol->max_iterations += 50;
		regenerate_color_palette(fractol);
		printf("Max iterations: %u\n", fractol->max_iterations);
	}
	else if (keycode == I_KEY)
	{
		fractol->inverted_colors = !fractol->inverted_colors;
		regenerate_color_palette(fractol);
	}
	else if (keycode == ESC_KEY)
	{
		delete_fractol(fractol);
		exit(0);
	}
	else
		key_event_and_the_azkaban_prisioner(keycode, fractol);
}

/*
 * This function is meant to be a handler for mlx_key_hook().
 */
int	key_event(int keycode, struct s_fractol *fractol)
{
	if (keycode == EQUALS_KEY && fractol->zoom > 0.000001L)
	{
		fractol->zoom /= 1.1L;
		fractol->h_offset *= 1.1L;
		fractol->v_offset *= 1.1L;
	}
	else if (keycode == MINUS_KEY)
	{
		fractol->zoom *= 1.1L;
		fractol->h_offset /= 1.1L;
		fractol->v_offset /= 1.1L;
	}
	else if (keycode == UP_KEY)
		fractol->v_offset += 50;
	else if (keycode == DOWN_KEY)
		fractol->v_offset -= 50;
	else if (keycode == LEFT_KEY)
		fractol->h_offset -= 50;
	else if (keycode == RIGHT_KEY)
		fractol->h_offset += 50;
	else
		key_event_and_the_secrets_chamber(keycode, fractol);
	update_fractal(fractol);
	return (0);
}
