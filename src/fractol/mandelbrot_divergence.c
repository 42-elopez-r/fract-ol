/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot_divergence.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 17:13:33 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 15:08:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <t_complex.h>

/*
 * This function initializes C scalled to the screen size.
 */
static void	initialize_c(t_complex *c, unsigned int x, unsigned int y,
		struct s_fractol *fractol)
{
	const long double	step_x = 4.0 / fractol->screen->width * fractol->zoom;
	const long double	step_y = 4.0 / fractol->screen->height * fractol->zoom;

	c->r = (x - fractol->screen->width / 2.0 + fractol->h_offset) * step_x;
	c->i = (fractol->screen->height / 2.0 - y + fractol->v_offset) * step_y;
}

/*
 * This function calculates the divergence in the Mandelbrot set for the (x,y)
 * window coordinates.
 */
int	mandelbrot_divergence(unsigned int x, unsigned int y,
		struct s_fractol *fractol)
{
	t_complex		c;
	t_complex		z;
	t_complex		z_prev;
	unsigned int	it;

	initialize_c(&c, x, y, fractol);
	z.r = 0;
	z.i = 0;
	it = 0;
	while (it < fractol->max_iterations)
	{
		if (!is_module_smaller_or_equal_than_two(&z))
			return (it);
		z_prev = z;
		z.r = (z.r * z.r) - (z.i * z.i) + c.r;
		z.i = 2 * z_prev.r * z_prev.i + c.i;
		it++;
	}
	return (-1);
}
