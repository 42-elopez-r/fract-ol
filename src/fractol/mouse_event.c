/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 18:52:02 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 20:36:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

/*
 * This function is meant to be a handler for mlx_mouse_hook().
 */
int	mouse_event(int button, int x, int y,
		struct s_fractol *fractol)
{
	if (button == 4 && fractol->zoom > 0.000001L)
	{
		fractol->h_offset -= fractol->screen->width / 2 - x;
		fractol->v_offset += fractol->screen->height / 2 - y;
		fractol->zoom /= 1.1L;
		fractol->h_offset *= 1.1L;
		fractol->v_offset *= 1.1L;
	}
	else if (button == 5)
	{
		fractol->h_offset -= fractol->screen->width / 2 - x;
		fractol->v_offset += fractol->screen->height / 2 - y;
		fractol->zoom *= 1.1L;
		fractol->h_offset /= 1.1L;
		fractol->v_offset /= 1.1L;
	}
	else if (button == 1)
	{
		fractol->h_offset -= fractol->screen->width / 2 - x;
		fractol->v_offset += fractol->screen->height / 2 - y;
	}
	update_fractal(fractol);
	return (0);
}
