/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   regenerate_color_palette.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/03 11:46:17 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/04 14:03:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <minilibx.h>
#include <libft.h>
#include <stdlib.h>

/*
 * This function allocates a new color palette with a lenth of
 * fractol->max_iterations returning true on success.
 */
static bool	allocate_color_palette(struct s_fractol *fractol)
{
	fractol->color_palette = ft_calloc(fractol->max_iterations,
			sizeof(unsigned int));
	if (!fractol->color_palette)
	{
		ft_putendl_fd("Memory allocation error", 2);
		return (false);
	}
	return (true);
}

/*
 * This function calculates the color corresponding to the given divergence.
 */
static t_color	calculate_color(int divergence, struct s_fractol *fractol)
{
	int		r;
	int		g;
	int		b;
	double	ratio;

	ratio = 2.0l * divergence / fractol->max_iterations;
	b = ft_max_int(0, 255 * (1 - ratio));
	r = ft_max_int(0, 255 * (ratio - 1));
	g = 255 - b - r;
	if (fractol->inverted_colors)
		return (0x00FFFFFF - rgba_to_color(r, g, b, 0));
	else
		return (rgba_to_color(r, g, b, 0));
}

/*
 * After deallocating the previous color palette, it generates a new one
 * based on fractol->max_iterations.
 */
bool	regenerate_color_palette(struct s_fractol *fractol)
{
	unsigned int	i;

	free(fractol->color_palette);
	if (!allocate_color_palette(fractol))
		return (false);
	i = 0;
	while (i < fractol->max_iterations)
	{
		fractol->color_palette[i] = calculate_color(i, fractol);
		i++;
	}
	return (true);
}
