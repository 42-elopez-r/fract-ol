/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_fractal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 16:57:44 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/05 13:33:54 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

/*
 * This function will update the fractal displayed according to the passed
 * data, or it will die in the attempt.
 */
void	update_fractal(struct s_fractol *fractol)
{
	int	x;
	int	y;
	int	divergence;

	x = 0;
	while (x < fractol->screen->width)
	{
		y = 0;
		while (y < fractol->screen->height)
		{
			if (fractol->fractal_type == MANDELBROT)
				divergence = mandelbrot_divergence(x, y, fractol);
			else if (fractol->fractal_type == BRANDELMOT)
				divergence = brandelmot_divergence(x, y, fractol);
			else
				divergence = julia_divergence(x, y, fractol);
			draw_pixel(divergence, x, y, fractol);
			y++;
		}
		x++;
	}
	update_screen(fractol->screen);
}
