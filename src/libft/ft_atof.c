/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 17:43:25 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/01 19:14:04 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

/*
 * This function moves back one position all the characters from a string
 * starting on a dot (MUST be checked before calling).
 */
static void	erase_dot(char *str)
{
	unsigned int	i;

	i = ft_strchr(str, '.') - str;
	while (str[i])
	{
		str[i] = str[i + 1];
		i++;
	}
}

/*
 * Calculates 10 to the power of exp.
 */
static double	power_of_ten(unsigned int exp)
{
	double	power;

	power = 1;
	while (exp--)
		power *= 10;
	return (power);
}

/*
 * Returns double conversion of the passed string.
 */
double	ft_atof(const char *str)
{
	char			*str_no_dot;
	unsigned int	dot_index;
	double			n;

	if (!ft_strchr(str, '.'))
		return (ft_atoi(str));
	str_no_dot = ft_strdup(str);
	if (!str_no_dot)
		return (0.0l);
	dot_index = ft_strchr(str, '.') - str;
	erase_dot(str_no_dot);
	n = ft_atoi(str_no_dot);
	free(str_no_dot);
	return (n / power_of_ten(ft_strlen(str) - 1 - dot_index));
}
