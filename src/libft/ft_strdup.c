/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 11:27:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/01 18:35:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strdup(const char *s)
{
	char	*new_str;
	int		i;

	new_str = malloc(ft_strlen(s) + 1);
	if (new_str)
	{
		i = 0;
		while (s[i])
		{
			new_str[i] = s[i];
			i++;
		}
		new_str[i] = '\0';
		return (new_str);
	}
	else
		return (NULL);
}
