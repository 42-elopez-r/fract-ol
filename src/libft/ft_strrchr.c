/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 17:21:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/31 22:36:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strrchr(const char *s, int c)
{
	char	*chr;
	int		keep;

	chr = NULL;
	keep = 1;
	while (keep)
	{
		if ((unsigned char)*s == (unsigned char)c)
			chr = ((char *)s);
		if (!*s)
			keep = 0;
		s++;
	}
	return (chr);
}
