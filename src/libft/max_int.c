/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max_int.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/03 13:20:29 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/03 13:21:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
 * I think its clear what this function does.
 */
int	ft_max_int(int a, int b)
{
	if (a > b)
		return (a);
	else
		return (b);
}
