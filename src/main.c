/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/03 16:32:21 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/04 21:45:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <args.h>
#include <fractol.h>
#include <mlx.h>

int	main(int argc, char *argv[])
{
	struct s_args		args;
	struct s_fractol	*fractol;

	if (!parse_args(argc, argv, &args))
		return (1);
	fractol = init_fractol(&args);
	if (!fractol)
		return (1);
	update_fractal(fractol);
	mlx_key_hook(fractol->screen->mlx_win, key_event, fractol);
	mlx_mouse_hook(fractol->screen->mlx_win, mouse_event, fractol);
	mlx_loop(fractol->screen->mlx);
	delete_fractol(fractol);
	return (0);
}
