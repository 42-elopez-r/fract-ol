/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 15:02:08 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/02 15:02:53 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <minilibx.h>
#include <libft.h>

void	print_error(const char *error)
{
	ft_putendl_fd(error, 2);
}
