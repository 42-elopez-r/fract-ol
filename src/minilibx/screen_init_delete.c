/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen_init_delete.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/17 17:20:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/04 16:33:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <minilibx.h>
#include <libft.h>
#include <stdlib.h>
#include <stdbool.h>

/*
** Initialize screen->mlx and screen->mlx_win. Returns true if success.
** If an error happens, it frees the screen and return false.
*/

static bool	init_window(t_screen *screen)
{
	screen->mlx = mlx_init();
	if (!screen->mlx)
	{
		print_error("Failed mlx_init()");
		free(screen);
		return (false);
	}
	screen->mlx_win = mlx_new_window(screen->mlx, screen->width, screen->height,
			PROG_NAME);
	if (!screen->mlx_win)
	{
		print_error("Failed window creation");
		free(screen);
		return (false);
	}
	return (true);
}

/*
** Initialize screen->buffer (needs screen->width and screen->height rightly
** set). Returns true if success. If an error happens, it frees the screen
** and returns false.
*/

static bool	init_buffer(t_screen *screen)
{
	int	i;

	screen->buffer = ft_calloc(screen->width, sizeof(t_color *));
	if (screen->buffer)
	{
		i = 0;
		while (i < screen->width)
		{
			screen->buffer[i] = ft_calloc(screen->height, sizeof(t_color));
			if (!screen->buffer[i])
			{
				print_error("Failed memory reservation for screen buffer");
				delete_screen(screen, i);
				return (false);
			}
			i++;
		}
		return (true);
	}
	else
	{
		print_error("Failed memory reservation for screen buffer");
		delete_screen(screen, 0);
		return (false);
	}
}

/*
** Initialize screen->displayed (needs screen->mlx, screen->width
** and screen->height rightly set). Returns true if success. If an error
** happens, it frees the screen and returns false.
*/

static bool	init_displayed(t_screen *screen)
{
	screen->displayed.img = mlx_new_image(screen->mlx, screen->width,
			screen->height);
	if (screen->displayed.img)
	{
		screen->displayed.addr = mlx_get_data_addr(screen->displayed.img,
				&screen->displayed.bits_per_pixel,
				&screen->displayed.line_length,
				&screen->displayed.endian);
		return (true);
	}
	else
	{
		print_error("Failed creation of image screen buffer");
		delete_screen(screen, 0);
		return (false);
	}
}

/*
** Initializes the t_screen struct returning it if success. It won't create a
** window nor create the displayed image if show_window is false.
** Returns NULLif an error happened
*/

t_screen	*init_screen(int width, int height)
{
	t_screen	*screen;

	screen = malloc(sizeof(t_screen));
	if (screen)
	{
		screen->width = width;
		screen->height = height;
		if (!init_window(screen))
			return (NULL);
		if (!init_buffer(screen))
			return (NULL);
		screen->displayed.img = NULL;
		if (!init_displayed(screen))
			return (NULL);
	}
	return (screen);
}

/*
** if the reservation of columns in the buffer failed, set max_buf_col to
** the failed index, otherwise just set it to 0
*/

void	delete_screen(t_screen *screen, int max_buf_col)
{
	int	max;
	int	i;

	if (screen)
	{
		max = tern_int(max_buf_col == 0, screen->width, max_buf_col);
		if (screen->displayed.img)
			mlx_destroy_image(screen->mlx, screen->displayed.img);
		if (screen->mlx_win)
			mlx_destroy_window(screen->mlx, screen->mlx_win);
		if (screen->mlx)
		{
			mlx_destroy_display(screen->mlx);
			free(screen->mlx);
		}
		if (screen->buffer)
		{
			i = -1;
			while (++i < max)
				free(screen->buffer[i]);
			free(screen->buffer);
		}
		free(screen);
	}
}
