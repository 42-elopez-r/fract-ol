/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_module_smaller_or_equal_than_two.c              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/02 17:53:58 by elopez-r          #+#    #+#             */
/*   Updated: 2021/08/02 17:54:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <t_complex.h>

/*
 * This function is equivalent to: |c| <= 2
 */
bool	is_module_smaller_or_equal_than_two(const t_complex *c)
{
	return ((c->r * c->r) + (c->i * c->i) <= 4);
}
